// reservations.js
// This is the controller that handles the groups of the user.
// Selecting a group will open the group chat.
app.controller('reservationsController', function($scope, $state, $localStorage, Utils, Popup, $filter, Watchers, $timeout, $ionicPlatform, Service, $window, $stateParams, $ionicTabsDelegate, $cordovaBarcodeScanner,$http, $cordovaSocialSharing) {
  $scope.$on('$ionicView.enter', function() {

    if($localStorage.restaurant && $localStorage.categories && $localStorage.goScanButton){
      $scope.restaurant = $localStorage.restaurant;
      $scope.categories = $localStorage.categories;
    }else{
      $localStorage.goScanButton = false;
    }

    //Notify whenever there are new messages.
    $scope.$watch(function() {
      return Service.getUnreadMessages();
    }, function(unreadMessages) {
      $scope.unreadMessages = unreadMessages;
    });

    //Update profile whenever there are changes done on the profile.
    $scope.$watch(function() {
      return Service.getProfile();
    }, function(profile) {
      $scope.profile = Service.getProfile();
    });

    //Notify whenever there are new friend requests.
    $scope.$watch(function() {
      return Service.getFriendRequestsCount();
    }, function(friendRequests) {
      $scope.friendRequestsCount = friendRequests;
    });

    //Notify whenever there are new group messages.
    $scope.$watch(function() {
      return Service.getUnreadGroupMessages();
    }, function(unreadGroupMessages) {
      $scope.unreadGroupMessages = unreadGroupMessages;
    });

    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = true;
    //Select the 4th tab on the footer to highlight the profile icon.
    $ionicTabsDelegate.select(2);
  });

  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      event.preventDefault();
    }
  });

  //Allow changing to other views when tabs is selected.
  $scope.changeTab = function(stateTo) {
    $scope.canChangeView = true;
    $state.go(stateTo);
  };


  $scope.tables = [1,2,3];


  $scope.help = function(stateTo) {
    Utils.message('','Funzione non ancora disponibile');
  };


  $scope.menuRestaurant = function(stateTo) {
    $state.go('menuRestaurant');
  };

  $scope.OtherShare = function(restaurant){
    window.plugins.socialsharing.share(restaurant.name+' - '+restaurant.address+' - '+restaurant.place+'('+restaurant.city+')', null, null, 'https://www.google.com/maps/?q='+restaurant.latitude+','+restaurant.longitude)
    .then(function(result) {
      Utils.message('','success');
    }, function(err) {
      Utils.message('','Failure');
    });
  }


  $scope.getGoScanButton = function() {
    return $localStorage.goScanButton;
  };

  $scope.setGoScanButton = function() {
    $scope.goScanButton = $localStorage.goScanButton;
  };

  $scope.Yappa = function() {
    $scope.canChangeView = true;
    $state.go('yappa');
  }

  $scope.scanBarcode = function(tables) {
    Utils.show();
    $cordovaBarcodeScanner.scan().then(function(imageData) {
      if(imageData.format)
      var table = imageData.text;

      var req = {
        method: 'POST',
        url: 'http://www.piu18.com/API/',
        data: {
          "method": 'qrCode',
          "tableId": table
        },
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }

      $http(req).then(function(response){
        console.log(response);

        if(!response.data.restaurant){
          $localStorage.goScanButton = false;
          Utils.message('','QRCode non esistente');
          return false;
        }

        if(response.data.categories.length==0){
          $localStorage.goScanButton = false;
          Utils.message('','Ordinazione non disponibile in questo momento');
          return false;
        }

        if ($localStorage.accountId) {
          firebase.database().ref('accounts/' + $localStorage.accountId).update({
            table: table
          });
        }

        $localStorage.tableId = table;
        $localStorage.restaurant =  response.data.restaurant;
        $localStorage.categories = response.data.categories;
        $localStorage.goScanButton = true;
        $scope.restaurant = response.data.restaurant;
        $scope.categories = response.data.categories;
        $scope.setGoScanButton();
        Utils.hide();

      }, function(response){
        Utils.hide();
      });

    }, function(error) {
      console.log("An error happened -> " + error);
    });
  };



})
