// register.js
// This is the controller that handles the registration of the user through Firebase.
// When the user is done registering, the user is automatically logged in.
app.controller('registerController', function($scope, $http, $state, $localStorage, Utils, Popup, $window) {
  $scope.$on('$ionicView.enter', function() {
    //Clear the Registration Form.
    $scope.user = {
      username: '',
      name: '',
      email: '',
      password: '',
      sex: '',
      table: '',
      zodiac: '',
      profilePic: 'img/profile.png'
    };

    $scope.profilePic = 'img/profile.png';
    $scope.zodiacPic = '';
    $scope.zodiacoverlay = false;
  });


  $scope.openZodiac = function() {
    $scope.zodiacoverlay = true;
  }


  $scope.changeZodiac = function(zodiac) {
    $scope.zodiacoverlay = false;
    $scope.user.zodiac = zodiac;
    $scope.user.profilePic = 'img/zodiac/'+zodiac+'.png';
    $scope.profilePic = 'img/zodiac/'+zodiac+'.png';
    $scope.zodiacPic = 'img/zodiac/'+zodiac+'.png';
  }



  $scope.register = function(user) {
    //Check if form is filled up.
    if (angular.isDefined(user)) {
      Utils.show();
      firebase.database().ref('accounts').orderByChild('email').equalTo(user.email).once('value').then(function(accounts) {
        if (accounts.exists()) {
          Utils.message(Popup.errorIcon, Popup.emailAlreadyExists);
        } else {
          firebase.database().ref('accounts').orderByChild('username').equalTo(user.username).once('value').then(function(accounts) {
            if (accounts.exists()) {
              Utils.message(Popup.errorIcon, Popup.usernameAlreadyExists);
            } else {
              //Create Firebase account.
              firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
              .then(function() {
                //Add Firebase account reference to Database. Firebase v3 Implementation.
                firebase.database().ref().child('accounts').push({
                  name: user.name,
                  username: user.username,
                  profilePic: user.profilePic,
                  email: user.email,
                  sex: user.sex,
                  zodiac: user.zodiac,
                  table: '',
                  userId: firebase.auth().currentUser.uid,
                  dateCreated: Date(),
                  provider: 'Firebase'
                }).then(function(response) {

                  var req = {
                    method: 'POST',
                    url: 'http://www.piu18.com/API/',
                    data: {
                      "method": 'addClient',
                      "name": user.name,
                      "username": user.username,
                      "password": user.password,
                      "profilePic": user.profilePic,
                      "email": user.email,
                      "sex": user.sex,
                      "zodiac": user.zodiac,
                      "userId": firebase.auth().currentUser.uid,
                   },
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                  }
                  $http(req).then(function(response){
                  }, function(response){
                  });

                  //Account created successfully, logging user in automatically after a short delay.
                  Utils.message(Popup.successIcon, Popup.accountCreateSuccess)
                  .then(function() {
                    getAccountAndLogin(response.key);
                  })
                  .catch(function() {
                    //User closed the prompt, proceed immediately to login.
                    getAccountAndLogin(response.key);
                  });
                  $localStorage.loginProvider = "Firebase";
                  $localStorage.email = user.email;
                  $localStorage.password = user.password;
                });
              })
              .catch(function(error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                //Show error message.
                switch (errorCode) {
                  case 'auth/email-already-in-use':
                  Utils.message(Popup.errorIcon, Popup.emailAlreadyExists);
                  break;
                  case 'auth/invalid-email':
                  Utils.message(Popup.errorIcon, Popup.invalidEmail);
                  break;
                  case 'auth/operation-not-allowed':
                  Utils.message(Popup.errorIcon, Popup.notAllowed);
                  break;
                  case 'auth/weak-password':
                  Utils.message(Popup.errorIcon, Popup.weakPassword);
                  break;
                  default:
                  Utils.message(Popup.errorIcon, Popup.errorRegister);
                  break;
                }
              });
            }
          });
        }
      });
    }
  };

  //Function to retrieve the account object from the Firebase database and store it on $localStorage.account.
  getAccountAndLogin = function(key) {
    $localStorage.accountId = key;
    firebase.database().ref('accounts/' + key).on('value', function(response) {
      var account = response.val();
      $localStorage.account = account;
      delete $localStorage.watcherAttached;
      $state.go('suggest');
    });
  };

  $scope.back = function() {
    $state.go('login');
  };
});
