// yappTo.js
// This is the controller that handles the groups of the user.
// Selecting a group will open the group chat.
app.controller('yappToController', function($scope, $state, $localStorage, Utils, Popup, $filter, Watchers, $timeout, $ionicPlatform, Service, $window, $stateParams, $ionicTabsDelegate, $cordovaBarcodeScanner,$http, $cordovaSocialSharing) {

  $scope.$on('$ionicView.enter', function() {

    $scope.mode = 'yappTo';


    if(!$localStorage.goScanButton || !$localStorage.orderId){
      $scope.showYappTo = false;
      $scope.canChangeView = true;
      Utils.message('',"E' necessario ordinare per poter abilitare la funzione conosciamoci");
      $state.go('reservations');
    }else{
      $scope.showYappTo = true;
    }

    $scope.chatUsers = [];

    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;

    //Select the 3rd tab on the footer to highlight the friends icon.
    $ionicTabsDelegate.select(0);
  });

  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      if(fromState.name=='yappTo' && toState.name=='reservations'){
        //    event.preventDefault();
      }
    }
  });


  //Allow changing to other views when tabs is selected.
  $scope.changeTab = function(stateTo) {
    $scope.canChangeView = true;
    $state.go(stateTo);
  };

  //Allow changing to other views when tabs is selected.
  $scope.requestSent = function() {
    $timeout(function() {
      Utils.message('', 'Richiesta di amicizia inviata con successo');
    }, 1000);
  };

  Utils.show();


  $scope.restaurant = $localStorage.restaurant;
  var req = {
    method: 'POST',
    url: 'http://www.piu18.com/API/',
    data: {
      "method": 'getTables',
      "restaurantId": $scope.restaurant.id
    },
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  }
  $http(req).then(function(response){
    $scope.tables = response.data.tables;
    Utils.hide();
  }, function(response){
    Utils.hide();
  });


  $scope.tableUsers = function(table) {
    $scope.table = table;
    $scope.chatUsers = Service.getTableUsers(table.id,$localStorage.accountId);
    $scope.chatUsersOverlay = true;
  }

  $scope.chatUsersOverlayClose = function(table) {
    $scope.chatUsersOverlay = false;
  }



  //Send Friend Request, create Firebase Database data.
  $scope.chat = function(friend) {
    $localStorage.friendId = friend.id;
    $scope.canChangeView = true;
    $state.go('message');
  }

  //Send Friend Request, create Firebase Database data.
  $scope.friendRequest = function(user) {
    $state.go('yappTo');

    $scope.requestSent();
    firebase.database().ref('accounts/' + user.id).once('value', function(account) {
      var friendRequests = account.val().friendRequests;
      if (!friendRequests) {
        friendRequests = [];
      }
      friendRequests.push($localStorage.accountId);
      friendRequests = friendRequests.filter(function(elem, index, self) {
        return index == self.indexOf(elem);
      })
      firebase.database().ref('accounts/' + user.id).update({
        friendRequests: friendRequests
      });
    });

    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      var requestsSent = account.val().requestsSent;
      if (!requestsSent) {
        requestsSent = [];
      }
      requestsSent.push(user.id);
      requestsSent = requestsSent.filter(function(elem, index, self) {
        return index == self.indexOf(elem);
      })
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        requestsSent: requestsSent
      });
    });

  };

});
