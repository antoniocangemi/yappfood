// yappa.js
// This is the controller that handles the groups of the user.
// Selecting a group will open the group chat.
app.controller('yappaController', function($scope, $state, $ionicHistory, $cordovaCamera, $localStorage, Utils, Popup, $timeout, Service, $ionicTabsDelegate,$http,$cordovaGeolocation) {

  $scope.$on('$ionicView.enter', function() {
    $scope.canChangeView = false;
    $scope.startCamera();
  });


  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      event.preventDefault();
    }
  });

  //Allow changing to other views when tabs is selected.
  $scope.changeTab = function(stateTo) {
    $scope.canChangeView = true;
    $state.go(stateTo);
  };

  $scope.yappaDescription = '';
  $scope.yappaSrc = "";


$scope.infoAction = function(){
  if($scope.infoShow){
    $scope.infoShow = false
  }else{
  $scope.infoShow = true;
}
}

  $scope.suggestAction = function(){
    var address = document.getElementById('address');
    var nameplace = document.getElementById('nameplace');
    var city = document.getElementById('city');

    if($scope.suggest != true){
      $scope.suggest = true;
      $scope.full = true;
    }else if($scope.suggest == true && address.value.length >=4 && nameplace.value.length >=4 && city.value.length >=4 ){


      Utils.show();
      var fileURI = $scope.yappaSrc;
      var options = new FileUploadOptions();
      options.fileKey="yappafile";
      options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
      options.mimeType="image/jpeg";
      options.chunkedMode = false;
      var params = {};
      params.method = 'suggest';
      params.address = address.value;
      params.nameplace = nameplace.value;
      params.city = city.value;
      params.accountId = $localStorage.accountId;
      options.params = params;
      var ft = new FileTransfer();
      ft.upload(fileURI, encodeURI("http://www.piu18.com/API/"), onUploadSuccess, onUploadFail, options);


      function onUploadSuccess(succ){
        Utils.hide();
        Utils.message("ion-camera","Suggerimento inserito con con successo");

        $scope.full = false;
        $scope.suggest = false;

        address.value = '';
        nameplace.value = '';
        city.value = '';
        $scope.back();
      }

      function onUploadFail(fail){
        Utils.hide();
        Utils.message("ion-error","Errore nel caricamento della foto");
        $scope.full = false;
        $scope.suggest = false;

        address.value = '';
        nameplace.value = '';
        city.value = '';
        $scope.back();
      }







    }else if($scope.suggest == true){
      Utils.message("","Inserisci i dati richiesti");
    }

  }


  $scope.yappaAction = function(){
    var message = document.getElementById('yappaDescription').value;

    if(message.length < 5){
      Utils.message("ion-camera","Lascia quì un commento...");
      return false;
    }

    Utils.show();
    var fileURI = $scope.yappaSrc;
    var options = new FileUploadOptions();
    options.fileKey="yappafile";
    options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
    options.mimeType="image/jpeg";
    options.chunkedMode = false;
    var params = {};
    params.method = 'yappa';
    params.message = message;
    params.accountId = $localStorage.accountId;
    options.params = params;
    var ft = new FileTransfer();
    ft.upload(fileURI, encodeURI("http://www.piu18.com/API/"), onUploadSuccess, onUploadFail, options);


    function onUploadSuccess(succ){
      Utils.hide();
      Utils.message("ion-camera","Yappata avvenuta con successo");
      $scope.back();
    }

    function onUploadFail(fail){
      Utils.hide();
      Utils.message("ion-error","Errore nel caricamento della foto");
    }

    //    $scope.changeTab('reservations');
  }


  $scope.back = function(){
    if($scope.suggest){
      $scope.full = false;
      $scope.suggest = false;
      $scope.changeTab('yappa');
    }else{
      $scope.changeTab('suggest');
    }
  }

  $scope.startCamera = function(){
    //cordova.plugins.camerapreview.startCamera({x: 0, y: 0, width: screen.width, height: screen.height}, "rear", false, false, false);

    var options = {
      quality: 99,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: false,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: screen.width,
      targetHeight: screen.height,
      saveToPhotoAlbum: false,
      correctOrientation:true
    };

    $cordovaCamera.getPicture(options).then(function(imageURI) {
      $scope.yappaSrc = imageURI;
    }, function(err) {
      // error
    });
  };
});
