// completeAccount.js
// This is the controller that handles the final steps when creating an account at Firebase when Social Login is used.
// The user is asked for their email address, because in some cases Social Login is not able to retrieve an email address or is not required by the service (such as Twitter).
// If the email is provided by the provider it is automatically filled in for them when the form loads.
'Use Strict';
app = angular.module('App.controllers',[])
