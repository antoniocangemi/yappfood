// findRestaurants.js
// This is the controller that handles the groups of the user.
// Selecting a group will open the group chat.
app.controller('findRestaurantsController', function($scope, $state, $localStorage, Utils, Popup, $timeout, Service, $ionicTabsDelegate,$http,$cordovaGeolocation,$cordovaLaunchNavigator,$compile) {


  $scope.$on('$ionicView.enter', function() {
    $scope.map = false;
    $scope.markerMe = null;
    $scope.infoRestaurant = false;



    $scope.restaurantInfo = function(restaurant){
      $scope.showMenu = false;
      $scope.showLavagna = false;

      if($scope.infoRestaurant == true){
        $scope.infoRestaurant = false;
      }else{
        $scope.reservation = false;
        $scope.infoRestaurant = true;
        $scope.restaurant = restaurant;

        $scope.restaurantName = restaurant.name;
        $scope.restaurantId = restaurant.idconfig;
        $scope.getMenu();
        $scope.getLavagna();

      }
    }



    $scope.getMenu = function() {
      Utils.show();

      var req = {
        method: 'POST',
        url: 'http://www.piu18.com/API/',
        data: {
          "method": 'getMenu',
          "restaurantId": $scope.restaurantId
        },
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }
      $http(req).then(function(response){
        //    $scope.categories = response.data.categories;
        //    $scope.subcategories = response.data.subcategories;
        $scope.products = response.data.products;
        if(response.data.products && response.data.products.length){
          $scope.showMenu = true;
        }else{
          $scope.showMenu = false;
        }
        Utils.hide();
      }, function(response){
        Utils.hide();
      });
    }

    $scope.getLavagna = function() {
      Utils.show();

      var req = {
        method: 'POST',
        url: 'http://www.piu18.com/API/',
        data: {
          "method": 'getLavagna',
          "restaurantId": $scope.restaurantId
        },
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }
      $http(req).then(function(response){
        //    $scope.categories = response.data.categories;
        //    $scope.subcategories = response.data.subcategories;
        $scope.lavagnaProducts = response.data.products;
        if(response.data.products && response.data.products.length){
          $scope.showLavagna = true;
        }else{
          $scope.showLavagna = false;
        }
        Utils.hide();
      }, function(response){
        Utils.hide();
      });
    }


    $scope.findRestaurant();

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10){
      dd='0'+dd
    }
    if(mm<10){
      mm='0'+mm
    }

    today = yyyy+'-'+mm+'-'+dd;
    document.getElementById("date_reservation").setAttribute("min", today);
    document.getElementById("date_reservation").value = today;

    $ionicTabsDelegate.select(1);


    $scope.data = {};
    $scope.data.currentPage = 0;






    var setupSlider = function() {
      //some options to pass to our slider
      $scope.data.sliderOptions = {
        loop: true,
        direction: 'horizontal', //or vertical
        speed: 300 //0.3s transition
      };

      //create delegate reference to link with slider
      $scope.data.sliderDelegate = null;

      //watch our sliderDelegate reference, and use it when it becomes available
      $scope.$watch('data.sliderDelegate', function(newVal, oldVal) {
        if (newVal != null) {
          $scope.data.sliderDelegate.on('slideChangeEnd', function() {
            $scope.data.currentPage = $scope.data.sliderDelegate.activeIndex;
            //use $scope.$apply() to refresh any content external to the slider
            $scope.$apply();
          });
        }
      });
    };

    setupSlider();

  });


  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      //      $scope.findRestaurant();
      //      event.preventDefault();
    }
  });

  //Allow changing to other views when tabs is selected.
  $scope.changeTab = function(stateTo) {
    $scope.canChangeView = true;
    $state.go(stateTo);
  };




  $scope.launchNavigator = function(latitude,longitude) {
    Utils.show();
    var destination = [latitude, longitude];
    $cordovaLaunchNavigator.navigate(destination).then(function() {
      console.log("Navigator launched");
      Utils.hide();
    }, function (err) {
      Utils.hide();
      console.error(err);
    });
    Utils.hide();

  };

  $scope.sharePosition = function(restaurant){
    Utils.message('','Attendere Prego...');
    window.plugins.socialsharing.share(restaurant.name+' - '+restaurant.address+' - '+'('+restaurant.city+')', null, null, 'https://www.google.com/maps/?q='+restaurant.latitude+','+restaurant.longitude)
    .then(function(result) {
      Utils.hide();
      Utils.message('','success');
    }, function(err) {
      Utils.message('','Failure');
      Utils.hide();
    });
    Utils.hide();

  }

  $scope.openInfo = function(id){

    for (i = 0; i < $scope.restaurants.length; i++) {
      if($scope.restaurants[i].id == id){
        $scope.restaurantInfo($scope.restaurants[i]);
        break;
      }
    }
  }





  $scope.reservation = false;

  $scope.showReservation = function(restaurant){
    if($scope.reservation){
      $scope.reservation = false;
    }else{
      $scope.reservation = true;
      $scope.infoRestaurant = false;
      $scope.restaurant = restaurant;
      $scope.restaurantName = restaurant.name;
      $scope.restaurantId = restaurant.id;
    }
  }

  $scope.reservationConfirm = function(){
    var number_client = document.getElementById("number_client");
    var date_reservation = document.getElementById("date_reservation");
    var time_reservation = document.getElementById("time_reservation");
    var special_request = document.getElementById("special_request");
    var promo = document.getElementById("promo");
    var name_confirm = document.getElementById("name_confirm");
    var email = document.getElementById("email");
    var tel = document.getElementById("tel");

    if(number_client.value.length && date_reservation.value.length && time_reservation.value.length && name_confirm.value.length && email.value.length && tel.value.length){
      Utils.show();

      var req = {
        method: 'POST',
        url: 'http://www.piu18.com/API/',
        data: {
          "method": 'sendReservation',
          "numberClient": number_client.value,
          "dateReservation": date_reservation.value,
          "timeReservation": time_reservation.value,
          "specialRequest": special_request.value,
          "promo": promo.value,
          "nameConfirm": name_confirm.value,
          "email": email.value,
          "tel": tel.value,
          "restaurantId": $scope.restaurantId
        },
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
      }

      $http(req).then(function(response){
        Utils.hide();
        $scope.reservation = false;
        Utils.message("","Prenotazione inviata correttamente. Entro pochi minuti riceverai una comunicazione da parte del gestore del locale.");

      }, function(response){
        Utils.hide();
      });


    }else{
      Utils.message("","E' necessario compilare i campi obbligatori.")
    }
  }


  $scope.getPosition = function(){
    var posOptions = {timeout: 10000, enableHighAccuracy: false};
    $cordovaGeolocation
    .getCurrentPosition(posOptions)
    .then(function (position) {
      var lat  = position.coords.latitude;
      var long = position.coords.longitude;
      $scope.getRestaurants(lat,long);
    }, function(err) {
    });

  }

  $scope.getRestaurants = function(lat,long){
    Utils.show();

    if ($scope.markers) {
      for (var i = 0; i < $scope.markers.length; i++) {
        $scope.markers[i].setMap(null);
      }
    }else{
      $scope.markers = [];
    }

    var req = {
      method: 'POST',
      url: 'http://www.piu18.com/API/',
      data: {
        "method": 'findRestaurants',
        "latitude": lat,
        "longitude": long,
      },
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }

    $http(req).then(function(response){
      $scope.restaurants = response.data.restaurants;

      var latLng = new google.maps.LatLng(lat, long);
      var myStyles =[
        {
          featureType: "poi",
          elementType: "labels",
          stylers: [
            { visibility: "off" }
          ]
        }
      ];

      var mapOptions = {
        center: latLng,
        zoom:16,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        rotateControl: false,
        fullscreenControl: true,
        styles: myStyles
      };

      if($scope.map == false){
        $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

        google.maps.event.addListener($scope.map,'dragend', function() {
          var coordinate = $scope.map.getCenter();
          $scope.getRestaurants(coordinate.lat(),coordinate.lng())
        });
      }



      google.maps.event.addListenerOnce($scope.map, 'idle', function(){




        if (!$scope.markerMe) {
          var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
          $scope.markerMe  = new google.maps.Marker({
            map: $scope.map,
            position: latLng,
          });


          var infoWindowMe = new google.maps.InfoWindow({
            content: $localStorage.account.name + ", tu sei qui!"
          });

          google.maps.event.addListener($scope.markerMe, 'click', function () {
            infoWindowMe.open($scope.map, $scope.markerMe);
          });
        }

        Utils.hide();


        $scope.infowindow = new google.maps.InfoWindow({
          content: '',
          pixelOffset: new google.maps.Size(100,0)
        });

        var marker, i;


        for (i = 0; i < $scope.restaurants.length; i++) {
          if($scope.restaurants[i].approved == '1'){
            var MarkerIcon = 'img/icon/marker_ristorante.png';
          }else{
            var MarkerIcon = 'img/icon/marker_suggerito.png';
          }
          var marker = new google.maps.Marker({
            position: new google.maps.LatLng($scope.restaurants[i].latitude, $scope.restaurants[i].longitude),
            map: $scope.map,
            icon: MarkerIcon
          });

          $scope.markers.push(marker);

          var info   = '';
          var content = '';
          var vetrina = '';


          if($scope.restaurants[i].vetrina ==  1){
            vetrina = '<div class="button icon ion-clipboard orange" ng-click="openInfo(' + $scope.restaurants[i].id + ')"> Menù del giorno </div>';
          }

          vetrina = vetrina + ' | <div class="button orange" ng-click="openInfo(' + $scope.restaurants[i].id + ') "> Info </div>';


          if($scope.restaurants[i].image){
            if($scope.restaurants[i].image.indexOf('http') == -1){
              var image = $scope.restaurants[i].pathIcon+''+$scope.restaurants[i].image;
            }else{
              var image = $scope.restaurants[i].image;
            }
            $scope.restaurants[i].image = image;
            content  = '<div class="infowindow"><img src="'+image+'" style="height:250px;width:auto"><br><div><strong>'+$scope.restaurants[i].name+'</strong></div><br><p>'+$scope.restaurants[i].address+'</p>'+vetrina+'</div>';
          }else{
            $scope.restaurants[i].image = 'img/logo.png';

            content  = '<div class="infowindow"><div><strong>'+$scope.restaurants[i].name+'</strong></div><br><p>'+$scope.restaurants[i].address+'</p>'+vetrina+'</div>';
          }


          var compiledContent  = $compile(content)($scope);

          google.maps.event.addListener(marker, 'click', (function(marker, content, scope) {
            return function() {
              scope.infowindow.setContent(content);
              scope.infowindow.open(scope.map, marker);
            };
          })(marker, compiledContent[0],  $scope));

        }

      });


    }, function(response){
      Utils.hide();
    });


    Utils.hide();
    $scope.navigateButton = true;

  }

  $scope.enableGPS = function() {
    cordova.plugins.diagnostic.switchToLocationSettings();
  }


  $scope.findRestaurant = function(){


    cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled){
      if(!enabled){
      }else{
        Utils.message('', 'Ricerca posizione in corso...');
      }
    }, function(error){
      $scope.enableGPS();
    });


    var watchOptions = {
      timeout : 500000,
      enableHighAccuracy: false // may cause errors if true
    };
    var watch = $cordovaGeolocation.watchPosition(watchOptions);
    watch.then(
      null,
      function(err) {
        Utils.message('', 'Abilita il GPS');
      },
      function(position) {
        var lat  = position.coords.latitude;
        var long = position.coords.longitude;
        $scope.getRestaurants(lat,long);
        watch.clearWatch();
      });

    }



    $scope.optionsOrder =  [
      { name : "Prezzo più alto", sortType : "price", sortReverse : true },
      { name : "Prezzo più basso", sortType : "price", sortReverse : false }
    ];

    $scope.optionsDistance =  [
      { name : "Più vicini", sortType : "distance", sortReverse : true }
    ];


    $scope.orderSelected = function(newValue, oldValue){
      $scope.sortType = newValue.sortType;
      $scope.sortReverse = newValue.sortReverse;
    };

    $scope.sortType     = 'price'; // set the default sort type
    $scope.sortReverse  = false;  // set the default sort order
    $scope.searchTerm   = '';     // set the default search/filter term

  });
