// menuRestaurant.js
// This is the controller that handles the groups of the user.
// Selecting a group will open the group chat.
app.controller('menuRestaurantController', function($scope, $state, $localStorage, Utils, Popup, $timeout, Service, $ionicTabsDelegate,$http) {

  $scope.$on('$ionicView.enter', function() {
    $localStorage.goScanButton = true;
    $scope.restaurant = $localStorage.restaurant;

    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = true;
    //Select the 4th tab on the footer to highlight the profile icon.
    $ionicTabsDelegate.select(0);


    $scope.data = {};
    $scope.data.currentPage = 0;



    var setupSlider = function() {
      //some options to pass to our slider
      $scope.data.sliderOptions = {
        loop: true,
        direction: 'horizontal', //or vertical
        speed: 300 //0.3s transition
      };

      //create delegate reference to link with slider
      $scope.data.sliderDelegate = null;

      //watch our sliderDelegate reference, and use it when it becomes available
      $scope.$watch('data.sliderDelegate', function(newVal, oldVal) {
        if (newVal != null) {
          $scope.data.sliderDelegate.on('slideChangeEnd', function() {
            $scope.data.currentPage = $scope.data.sliderDelegate.activeIndex;
            //use $scope.$apply() to refresh any content external to the slider
            $scope.$apply();
          });
        }
      });
    };

    setupSlider();

  });


  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      event.preventDefault();
    }
  });

  //Allow changing to other views when tabs is selected.
  $scope.changeTab = function(stateTo) {
    $scope.canChangeView = true;
    $state.go(stateTo);
  };

  Utils.show();
  $scope.totOrder = 0;


  $scope.restaurant = $localStorage.restaurant;
  var req = {
    method: 'POST',
    url: 'http://www.piu18.com/API/',
    data: {
      "method": 'getMenu',
      "restaurantId": $scope.restaurant.id
    },
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  }
  $http(req).then(function(response){
    $scope.categories = response.data.categories;
    $scope.subcategories = response.data.subcategories;
    $scope.products = response.data.products;

    for(var i = 0; i < $scope.categories.length; i++){
      if($scope.categories[0].id){
        $scope.changeCategory($scope.categories[0]);
      }
      break;
    }
    Utils.hide();
  }, function(response){
    Utils.hide();
  });

  $scope.yappTo = function(category) {
    $state.go('yappTo');
  }

  $scope.changeCategory = function(category) {
    $scope.categorySelected = category.id;
    $scope.subcategorySelected = '';
  };

  $scope.changeSubcategory = function(subcategory) {
    $scope.subcategorySelected = subcategory.id;
  }

  $scope.productMinus = function(product) {
    product.quantity = parseInt(product.quantity);
    if( product.quantity > 0 ){
      product.quantity = product.quantity-1;
    }
    $scope.calculateOrder();
  }

  $scope.productPlus = function(product) {
    product.quantity = parseInt(product.quantity);
    product.quantity = product.quantity +1;
    $scope.calculateOrder();
  }

  $scope.calculateOrder = function(){
    if(!$scope.products){
      return false;
    }
    var total = 0;
    for(var i = 0; i < $scope.products.length; i++){
      var product = $scope.products[i];

      total += (product.price * product.quantity);
    }
    $scope.totOrder = total;
  }
  $scope.order = [];




  $localStorage.orderId = "";


  $scope.sendOrder = function(){
    Utils.show();

    $scope.totOrder = 0;
    $scope.order.push(angular.copy($scope.products));

    $localStorage.order = $scope.order;

    var currentOrder = [];
    for(var i = 0; i < $scope.products.length; i++){
      var product = $scope.products[i];
      if(product.quantity>0){
        currentOrder.push(angular.copy(product));
        $scope.products[i].quantity = 0;
      }
    }
    $scope.listOrderExists = true;

    var req = {
      method: 'POST',
      url: 'http://www.piu18.com/API/',
      data: {
        'method': 'sendOrder',
        'order': currentOrder,
        'orderId': $localStorage.orderId,
        'tableId': $localStorage.tableId
      },
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }

    $http(req).then(function(response){
      $localStorage.orderId = response.data;
      Utils.hide();
      Utils.message("ion-android-restaurant","Congratulazioni, il tuo ordine è stato perso in carico");

    }, function(response){
      Utils.hide();
    });
  }


  $scope.askBill = function(){
    Utils.show();

    var req = {
      method: 'POST',
      url: 'http://www.piu18.com/API/',
      data: {
        'method': 'askBill',
        'orderId': $localStorage.orderId,
      },
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }

    $http(req).then(function(response){
      Utils.hide();
      $localStorage.restaurant = false;
      $localStorage.orderId = 0;
      $localStorage.tableId = 0;
      $state.go('reservations');
      Utils.message("ion-android-restaurant","Ti ringraziamo per averci scelto, entro pochi minuti riceverai il conto.");
      $localStorage.goScanButton = false;
    }, function(response){
      Utils.hide();
    });
  }


  $scope.exitRestaurant = function(){
    $localStorage.restaurant = false;
    $localStorage.orderId = 0;
    $localStorage.tableId = 0;
    $state.go('reservations');
    Utils.message("ion-android-restaurant","Hai abbandonato il tavolo...");
    $localStorage.goScanButton = false;

  }

  $scope.listOrder = function(){
    var order = '';
    for(var i = 0; i < $scope.order.length; i++){
      for(var c = 0; c < $scope.order[i].length; c++){
        var orders = $scope.order[i][c];
        if(orders.quantity > 0){
          order = order+"<p>"+orders.quantity+" "+orders.name+"</p>";
        }
      }
    }
    Utils.message("ion-android-restaurant",order);
  }
})
