// watchers.js
// This is the controller that handles real time synchronization with the Firebase database and broadcasts to the controller and interfaces with service.js.
// Every time a callback was triggered on Firebase, changes made are made to reflect our Service at service.js.
angular.module('App').factory('Watchers', function($localStorage, $filter, $timeout, Service, Utils, $rootScope,$cordovaLocalNotification) {
  return {
    //Watcher responsible for adding users on Firebase to the service.
    addUsersWatcher: function() {
      firebase.database().ref('accounts').on('child_added', function(account) {
        console.log('addUsersWatcher');
        Utils.show();
        var profile = {
          id: account.key,
          name: account.val().name,
          username: account.val().username,
          sex: account.val().sex,
          zodiac: account.val().zodiac,
          table: account.val().table,
          profilePic: account.val().profilePic,
          date: $filter('date')(new Date(account.val().dateCreated), 'MMM dd, yyyy'),
          provider: account.val().provider
        };
        $timeout(function() {
          Utils.hide();
          Service.addUser(profile);
        });
      });
    },
    //Watcher responsible for adding and updating the user's profile to the service.
    addProfileWatcher: function(accountId) {
      firebase.database().ref('accounts/' + accountId).on('value', function(account) {
        console.log('addProfileWatcher');
        Utils.show();
        var profile = {
          id: account.key,
          name: account.val().name,
          username: account.val().username,
          sex: account.val().sex,
          zodiac: account.val().zodiac,
          table: account.val().table,
          profilePic: account.val().profilePic,
          date: $filter('date')(new Date(account.val().dateCreated), 'MMM dd, yyyy'),
          provider: account.val().provider
        };
        $timeout(function() {
          Utils.hide();
          Service.setProfile(profile);
          //Add to excludedIds because own profile should not show up on search users.
          Service.addExcludedIds(profile.id);
        });
      });
    },
    //Watcher responsible for adding and updating conversations with the user to the service.
    addNewConversationWatcher: function(accountId) {

      firebase.database().ref('accounts/' + accountId).child('conversations').on('child_added', function(conversation) {
        console.log('addNewConversationWatcher');
        Utils.show();
        var conversationId = conversation.val().conversation;
        var friendId = conversation.val().friend;
        var messagesRead = conversation.val().messagesRead;
        firebase.database().ref('accounts/' + friendId).once('value', function(account) {
          firebase.database().ref('conversations/' + conversationId).once('value', function(conversation) {
            Utils.show();
            var messagesList = [];
            var messages = conversation.val().messages;
            for (var i = 0; i < messages.length; i++) {
              var profilePic, messageClass;
              if (messages[i].sender == $localStorage.accountId) {
                profilePic = Service.getProfile().profilePic;
                messageClass = 'self';
              } else {
                profilePic = Service.getFriend(messages[i].sender).profilePic;
                messageClass = 'other';
              }
              var message = {
                rawDate: new Date(messages[i].date),
                time: $filter('date')(new Date(messages[i].date), 'h:mm a'),
                date: $filter('date')(new Date(messages[i].date), 'MMM dd'),
                message: messages[i].message,
                image: messages[i].image,
                sender: messages[i].sender,
                type: messages[i].type,
                profilePic: profilePic,
                class: messageClass
              };
              messagesList.push(message);
            }
            var unreadMessages = messages.length - messagesRead;

            console.log("You Have "+unreadMessages+" New Messages");
            var message = messagesList[messagesList.length - 1];
            var lastMessage;

            if (message.type == 'text') {
              if (message.sender == $localStorage.accountId) {
                lastMessage = "You: " + message.message;
              } else {
                lastMessage = message.message;
              }
            } else {
              if (message.sender == $localStorage.accountId) {
                lastMessage = "You sent a photo message.";
              } else {
                lastMessage = "Sent you a photo message.";
              }
            }

            var conversation = {
              friend: Service.getFriend(friendId),
              messages: messagesList,
              unreadMessages: unreadMessages,
              lastMessage: lastMessage,
              id: conversationId,
              lastActiveDate: messagesList[messagesList.length - 1].rawDate
            };

            Service.addUnreadMessages(unreadMessages);
            $timeout(function() {
              Service.addConversation(conversation);
              Utils.hide();
              $rootScope.$broadcast('conversationAdded', {
                friendId: friendId
              });
            });

            //Watcher responsible when a new message was added to the conversation.
            firebase.database().ref('conversations/' + conversationId).on('child_changed', function(message) {
              console.log('conversationschild_changed');
            Utils.show();
              var message = message.val()[message.val().length - 1];

              var profilePic, messageClass;
              if (message.sender == $localStorage.accountId) {
                profilePic = Service.getProfile().profilePic;
                messageClass = 'self';
              } else {
                profilePic = Service.getFriend(message.sender).profilePic;
                messageClass = 'other';
                $cordovaLocalNotification.schedule({
                  id: Service.getFriend(message.sender).friendId,
                  title: 'YappFood',
                  text: "You Have New Messages From "+Service.getFriend(message.sender).username,
                  icon: 'res://icon.png',
                  smallIcon: 'res://icon.png',
                  led:"FA4866"
                }).then(function (result) {
                  console.log('Notification 1 triggered');
                });
              }
              var message = {
                time: $filter('date')(new Date(message.date), 'h:mm a'),
                date: $filter('date')(new Date(message.date), 'MMM dd'),
                message: message.message,
                image: message.image,
                sender: message.sender,
                type: message.type,
                profilePic: profilePic,
                class: messageClass
              };
              $timeout(function() {
                Utils.hide();
                Service.addMessageToConversation(conversationId, message);
                $rootScope.$broadcast('messageAdded');
              });
            });
          });
        });
      });

      //Watcher responsible for when the accountId's conversations get updated, which in this case was the unreadMessages.
      firebase.database().ref('accounts/' + accountId).child('conversations').on('child_changed', function(conversation) {
        console.log('conversationschild_changed');
        Utils.show();
        var conversationId = conversation.val().conversation;

        var messagesRead = Service.getConversationById(conversationId).unreadMessages;
        $timeout(function() {
          Utils.hide();
          Service.minusUnreadMessages(messagesRead);
          Service.setUnreadMessages(conversationId, 0);
        });
      });
    },
    //Watcher responsible for the user's friends.
    addNewFriendWatcher: function(accountId) {
      firebase.database().ref('accounts/' + accountId).child('friends').on('child_added', function(friendId) {
        console.log('addNewFriendWatcher');
        Utils.show();
        var friendId = friendId.val();
        firebase.database().ref('accounts/' + friendId).on('value', function(account) {
          Utils.show();
          var account = account.val();
          var friend = {
            profilePic: account.profilePic,
            name: account.name,
            username: account.username,
            id: friendId,
            online: account.online
          };
          $timeout(function() {
            Service.addOrUpdateFriend(friend);
            Service.updateConversationFriend(friend);
            Service.addExcludedIds(friend.id);
            Utils.hide();
          });
        });
      });
    },
    //Watcher responsible for friend requests.
    addFriendRequestsWatcher: function(accountId) {
      firebase.database().ref('accounts/' + accountId).child('friendRequests').on('child_added', function(friendId) {
        console.log('addFriendRequestsWatcher');
        Utils.show();
        var friendId = friendId.val();
        firebase.database().ref('accounts/' + friendId).once('value', function(account) {
          Utils.show();
          var account = account.val();
          var friendRequest = {
            profilePic: account.profilePic,
            name: account.name,
            username: account.username,
            id: friendId,
            online: account.online
          };
          $timeout(function() {
            Service.addFriendRequest(friendRequest);
            Service.addExcludedIds(friendRequest.id);
            Utils.hide();
          });
        });
      });
      //Watcher for friendRequests removed.
      firebase.database().ref('accounts/' + accountId).child('friendRequests').on('child_removed', function(friendId) {
        console.log('friendRequestschild_removed');
        Utils.show();
        var friendId = friendId.val();
        $timeout(function() {
          Service.removeFriendRequest(friendId);
          Service.removeFromExcludedIds(friendId);
          Utils.hide();
        });
      });
    },
    //Watcher responsible for friend requests sent.
    addRequestsSentWatcher: function(accountId) {
      firebase.database().ref('accounts/' + accountId).child('requestsSent').on('child_added', function(friendId) {
        console.log('addRequestsSentWatcher');
      Utils.show();
        var friendId = friendId.val();
        firebase.database().ref('accounts/' + friendId).once('value', function(account) {
          Utils.show();
          var account = account.val();
          var friendRequest = {
            profilePic: account.profilePic,
            name: account.name,
            username: account.username,
            id: friendId,
            online: account.online
          };
          $timeout(function() {
            Service.addRequestSent(friendRequest);
            Service.addExcludedIds(friendRequest.id);
            Utils.hide();
          });
        });
      });
      //Watcher for requests removed.
      firebase.database().ref('accounts/' + accountId).child('requestsSent').on('child_removed', function(friendId) {
        console.log('requestsSentchild_removed');
        Utils.show();
        var friendId = friendId.val();
        $timeout(function() {
          Service.removeRequestSent(friendId);
          Service.removeFromExcludedIds(friendId);
          Utils.hide();
        });
      });
    },
    //Watcher for new groups.
    addNewGroupWatcher: function(accountId) {
      firebase.database().ref('accounts/' + accountId).child('groups').on('child_added', function(group) {
        Utils.show();
        var groupId = group.val().group;
        var messagesRead = group.val().messagesRead;
        firebase.database().ref('groups/' + groupId).once('value', function(group) {
          var name = group.val().name;
          var image = group.val().image;

          var messagesList = [];
          var messages = group.val().messages;
          for (var i = 0; i < messages.length; i++) {
            var profilePic, messageClass;
            if (messages[i].sender == $localStorage.accountId) {
              profilePic = Service.getProfile().profilePic;
              messageClass = 'self';
            } else {
              profilePic = Service.getProfilePic(messages[i].sender);
              messageClass = 'other';

            }
            var message = {
              time: $filter('date')(new Date(messages[i].date), 'h:mm a'),
              date: $filter('date')(new Date(messages[i].date), 'MMM dd'),
              rawDate: new Date(messages[i].date),
              message: messages[i].message,
              image: messages[i].image,
              sender: messages[i].sender,
              type: messages[i].type,
              profilePic: profilePic,
              class: messageClass
            };
            messagesList.push(message);
          }
          var unreadMessages = messages.length - messagesRead;

          var group = {
            name: name,
            image: image,
            messages: messagesList,
            id: groupId,
            unreadMessages: unreadMessages,
            lastActiveDate: messagesList[messagesList.length - 1].rawDate
          };

          Service.addUnreadGroupMessages(unreadMessages);
          $timeout(function() {
            Service.addGroup(group);
            Utils.hide();
          });

          var usersList = [];
          //Watcher for new members on the group.
          firebase.database().ref('groups/' + groupId).child('users').on('child_added', function(userId) {
            Utils.show();
            var userId = userId.val();
            if(userId == $localStorage.accountId)
              usersList.push(Service.getProfile());
            else
              usersList.push(Service.getAccount(userId));
            $timeout(function() {
              Service.updateGroupUsers(groupId, usersList);
              Utils.hide();
            });
          });

          //Watcher for when members leave the group.
          firebase.database().ref('groups/' + groupId).child('users').on('child_removed', function(userId) {
            Utils.show();
            var userId = userId.val();
            $timeout(function() {
              Service.removeGroupUser(groupId, userId);
              Utils.hide();
            });
          });

          //Watcher for changes done on the group, messages and image.
          firebase.database().ref('groups/' + groupId).on('child_changed', function(change) {
            if (change.key == 'messages') {
              Utils.show();
              var message = change.val()[change.val().length - 1];
              var profilePic, messageClass;
              if (message.sender == $localStorage.accountId) {
                profilePic = Service.getProfile().profilePic;
                messageClass = 'self';
              } else {
                profilePic = Service.getProfilePic(message.sender);
                messageClass = 'other';
              }
              var message = {
                time: $filter('date')(new Date(message.date), 'h:mm a'),
                date: $filter('date')(new Date(message.date), 'MMM dd'),
                message: message.message,
                image: message.image,
                sender: message.sender,
                type: message.type,
                profilePic: profilePic,
                class: messageClass
              };
              $timeout(function() {
                Service.addMessageToGroup(groupId, message);
                $rootScope.$broadcast('messageAdded');
                Utils.hide();
              });
            } else if(change.key == 'image') {
              $timeout(function() {
                Service.setGroupImage(groupId, change.val());
              });
            }
          });
        });
      });
      //Watcher for groups removed.
      firebase.database().ref('accounts/' + accountId).child('groups').on('child_removed', function() {
        Utils.show();
        var groupId = $localStorage.groupId;
        $timeout(function() {
          Service.removeGroup(groupId);
          Utils.hide();
        });
      });
      //Watcher for user when new groupMessages were read.
      firebase.database().ref('accounts/' + accountId).child('groups').on('child_changed', function(group) {
        Utils.show();
        var groupId = group.val().group;
        var messagesRead = Service.getGroupById(groupId).unreadMessages;
        $timeout(function() {
          Service.minusUnreadGroupMessages(messagesRead);
          Service.setUnreadGroupMessages(groupId, 0);
          Utils.hide();
        });
      });
    }
  };
});
